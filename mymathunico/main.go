// Package mymath provides math solutions
package mymath

//Sum suma un número ilimitado de valores de tipo int.

func Sum(xi ...int) int {

	suma := 0
	for _, v := range xi {

		suma += v
	}
	return suma
}
